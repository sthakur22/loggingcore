#!/bin/bash
image=logging_core
container='logging_core'

docker rm -f $container

docker run --restart always -d --name $container --add-host wi01-oradb01.beyondbio.com:172.26.44.101 -p 5200:5066 -v ~/loggingcore/app:/home/web $image python run.py

