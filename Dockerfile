FROM continuumio/miniconda3:4.2.12
RUN mkdir -p /home && useradd -m -d /home/web web

RUN apt-get update
RUN apt-get -y install \
    libaio1 \
    build-essential \
    unzip

RUN mkdir -p opt/oracle
ADD ./libraries/ .

RUN unzip instantclient-basic-linux.x64-12.1.0.2.0.zip -d /opt/oracle \
  && unzip instantclient-sdk-linux.x64-12.1.0.2.0.zip -d /opt/oracle  \
  && mv /opt/oracle/instantclient_12_1 /opt/oracle/instantclient \
  && ln -s /opt/oracle/instantclient/libclntsh.so.12.1 /opt/oracle/instantclient/libclntsh.so \
  && ln -s /opt/oracle/instantclient/libocci.so.12.1 /opt/oracle/instantclient/libocci.so

ENV ORACLE_HOME="/opt/oracle/instantclient"
ENV ORACLE_LIB_DIR="/opt/oracle/instantclient"
ENV ORACLE_INCLUDE_DIR="/opt/oracle/instantclient/sdk/include"
ENV DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:$ORACLE_HOME
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME

RUN pip install --upgrade pip

RUN pip install -U \ 
    ldap3 \
    pyOpenSSL \
    flask==0.12.2 \
    flask-wtf==0.14.2 \
    flask-sqlalchemy==2.2 \
    flask-bootstrap==3.3.7.1 \
    flask-login==0.4.0 \
    flask-migrate==2.0.4 \
    wtforms-alchemy==0.16.2 \
    flask-admin==1.5.0 \
    gunicorn==19.7.1 \
    sqlalchemy_continuum==1.3.0 \
    cx_oracle==5.3


WORKDIR /home/web
#COPY app /home/web/.
ENV FLASK_APP=__init__.py

USER web
EXPOSE 5066
#CMD python test_db.py ; python run.py
