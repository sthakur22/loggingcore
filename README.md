# README #

This is a consolidated ldap (intrexon email) login application for DSCB web applications

### What is this repository for? ###

* Provides API for other web apps to securely support ldap login (intrexon email login) over SSL. Endpoint accessible at https://[hostaddress]:5200/login
* Provides web interface to see user priveleges for web apps and common project codes

### Important Notice ###
* This web application only supports https and not http for security reasons.
* When accessing the website via webbrowser one may encounter a 'privacy error' or similar message because the certificate is not recognized. Simply click advanced options and proceed.

### API Usage ###
* Supports POST endpoint at https://[hostaddress]:5200/login
* Must send json object with parameters sessionToken(currrently unused), username(email not including '@something.com'), and password
* Make sure request does not verify SSL certificate. Webapp currently has no usable certificate. See example below
* ex. res = requests.post(loginEndpoint, json={"sessionToken":"lalala",'username':email,'password':password},verify = False)
* API will reply back with json object containing boolean 'response'. If credentials are valid response will be True
* ex. if res.ok and res.json()['response']: --do something--


### How do I get set up? ###

* Clone the git repo
* Point the app to the database of choice
    * loggingcore/app/dscb_common/config.py
    * in the config file the mode can be set to switch between preset db connections

* Dependencies are all handled by the Dockerfile
* Deployment instructions
    * Build the Dockerfile   (bash build_docker.sh)
    * Run detached application (bash run_docker_detached.sh)
    * or run interactive application (bash run_docker_it.sh, python run.py)

### --Important Files--
    *the flask/python definitions for different webpages are in loggingcore/app/dscb_common/views.py
    *html templates used for the webpages are in loggingcore/app/dscb_common/templates
    *sql/database mapping defined in loggingcore/app/dscb_common/models.py
    *database server pointer is in loggingcore/app/dscb_common/config.py
    *wtforms(forms used to add/edit items) are in loggingcore/app/dscb_common/forms.py
    
### --Detached Version--  (use for 'production')
    * Modify run_docker_detached.sh so that the volume mapping corresponds to your host system file structure
    * go to loggingcore folder (cd exampleusr/loggingcore)
    * Run (bash run_docker_detached.sh)
    * Program automatically launches the server
    
### --Interactive Terminal Version (good for debugging)
    * Modify run_docker_it.sh so that the volume mapping corresponds to your host system file structure
    * Run (bash run_docker_it.sh)
    * Inside the container, run  (python run.py)

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact