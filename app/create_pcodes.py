from dscb_common import User, Pcode, db
import requests
import json
import re

print("grabbing pcodes from intrexon's website")
r=requests.get('https://mod.intrexon.com/web_services/program_codes_api/program_codes')

pcode_dict=r.json()    #type list of dictionary

counter = 0

for pcode in pcode_dict:
    counter+=1
    activeStatus = pcode['active'] == 'true'
    name = pcode['name']
    for i in range(len(name)):
        if (ord(name[i])>=128):
            name = name[:i] + str(chr(45)) + name[i+1:]
    db.session.add(Pcode(pcode['code'],name,activeStatus))

print("Adding "+str(counter)+" pcodes")

db.session.commit()

print(str(counter)+" pcodes created")

