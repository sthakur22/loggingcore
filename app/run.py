from dscb_common import app as application


if __name__ == '__main__':
    application.run(
        host='0.0.0.0',
        port=5066,
        threaded=True,
        ssl_context='adhoc'
    )

##form stuff
#prepopulate with allowed relationships data
#http://stackoverflow.com/questions/40554514/wtforms-alchemy-what-about-pre-populating-the-form-with-relationship-data
