from dscb_common import User, Pcode, db
from datetime import datetime
newUsers = []


'''
#unique appname and username test:
uniqueUser = User(username = "uniqueUsername",app_name = "app")
db.session.add(uniqueUser)
db.session.commit()

duplicateUser1 = User(username = "duplicateUsername",app_name = "app")

db.session.add(duplicateUser1)
db.session.commit()


duplicateUser2 = User(username = "duplicateUsername",app_name = "app")

db.session.add(duplicateUser2)
db.session.commit()

exit()
#end test
'''


def grabUsersFromFile(filename,app_name):
    with open(filename,'r') as userFile:
        line = userFile.readline()
        while line:
            userInfo = line.split(',')
            if(len(userInfo) != 4):
                raise ValueError('Bad line: '+str(userInfo))
            email = userInfo[0].split('@')[0].lower()
            active = bool(userInfo[1])
            privelege_level = int(userInfo[2])
            registered_on = None
            if(userInfo[3] != "None\n"):
                registered_on = datetime.strptime(userInfo[3][:len(userInfo[3])-2], "%Y-%m-%d %H:%M:%S")
            newUser = User(username = email, active = active, registered_on = registered_on, privelege_level = privelege_level, app_name = app_name)
            newUsers.append(newUser)
            line = userFile.readline()

grabUsersFromFile("dscb_users.txt","dscb_tracker")
grabUsersFromFile("mbc_users.txt","mbc_tracker")

print("Adding "+ str(len(newUsers)) + " users")

for user in newUsers:
    #print(user)
    db.session.add(user)
db.session.commit()
print(str(len(newUsers)) + " users added")
