import os
basedir = os.path.abspath(os.path.dirname(__file__))


modes = ['production','test','dev','ro','local']
mode = modes[2]



if mode == 'local':
    ###################################################################################################
    #local sqlite app
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'common_app.db')
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLITE_TEST = False
    BOOTSTRAP_SERVE_LOCAL = True

    ##################################################################################################



elif mode == 'production':
    ###################################################################################################
    #dscb_common_production

    SQLALCHEMY_DATABASE_URI = "oracle+cx_oracle://dscb_common_prod:h6C7m2s4A3n8@wi01-oradb01.beyondbio.com:1521/oraead"
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLITE_TEST = False
    BOOTSTRAP_SERVE_LOCAL = True

    ##################################################################################################

elif mode == 'test':
    ###################################################################################################
    #dscb_common_test

    SQLALCHEMY_DATABASE_URI = "oracle+cx_oracle://dscb_common_test:J9b3H6d5Z4R7@wi01-oradb01.beyondbio.com:1521/oraead"
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLITE_TEST = False
    BOOTSTRAP_SERVE_LOCAL = True

    ##################################################################################################


elif mode == 'dev':
    ###################################################################################################
    #dscb_common_test

    SQLALCHEMY_DATABASE_URI = "oracle+cx_oracle://dscb_common_dev:N3j5U8q6c7s9@wi01-oradb01.beyondbio.com:1521/oraead"
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLITE_TEST = False
    BOOTSTRAP_SERVE_LOCAL = True

    ##################################################################################################

elif mode == 'ro':
    ###################################################################################################
    #dscb_common_rro

    SQLALCHEMY_DATABASE_URI = "oracle+cx_oracle://dscb_common_ro:g4k7e3C9Z8K6@wi01-oradb01.beyondbio.com:1521/oraead"
    SQLALCHEMY_MIGRATE_REPO = 'db_repository'
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
    SQLITE_TEST = False
    BOOTSTRAP_SERVE_LOCAL = True

    ##################################################################################################











