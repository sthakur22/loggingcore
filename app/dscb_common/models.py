#from sqlalchemy import Column, Integer, String, Text, ForeignKey
#from database import Base
from dscb_common import db
from sqlalchemy_continuum import make_versioned
from sqlalchemy.orm import backref
from datetime import datetime, timedelta, date


####models#######
make_versioned()
TABLE_PREFIX = "common_"
class User(db.Model):
    __tablename__ = TABLE_PREFIX + "users"
    __versioned__ = {}
    id = db.Column(db.Integer, db.Sequence('user_seq', start=1, increment=1), primary_key=True)
    username = db.Column(db.String(120), nullable=False)
    app_name = db.Column(db.String(120), nullable=False)
    active = db.Column(db.Boolean, default=True)
    privelege_level = db.Column(db.Integer, nullable=False, default=1)
    registered_on = db.Column(db.DateTime)

    def __init__(self, username = None, app_name = None, privelege_level = 1,active = True, registered_on = datetime.utcnow()):
        if User.query.filter_by(username = username).filter_by(app_name = app_name).one_or_none():
            raise ValueError("User with app name and username already exists")
        self.app_name = app_name
        self.username = username
        self.active = active
        self.privelege_level = privelege_level
        self.registered_on = registered_on

    def __repr__(self):
        return (self.username + ', ' + self.app_name + ', ' + str(self.active) + ', ' + str(self.privelege_level) + ', ' + str(self.registered_on))




class Pcode(db.Model):
    __tablename__ = TABLE_PREFIX + "pcodes"
    __versioned__ = {}
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(150), nullable=False)
    active = db.Column(db.Boolean)

    def __init__(self, id, name, active):
        self.name = name
        self.id = id
        self.active=active
    def __repr__(self):
        return str(self.id) + " " + self.name

