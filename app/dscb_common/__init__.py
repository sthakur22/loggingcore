from flask import Flask
from flask_bootstrap import Bootstrap



####db######
import sqlalchemy as sa
########



from flask_sqlalchemy import SQLAlchemy

from flask_migrate import Migrate, MigrateCommand



bootstrap = Bootstrap()

app = Flask(__name__)
app.secret_key = 'dscbapps'


app.config.from_pyfile('config.py')
app.config['SQLALCHEMY_POOL_RECYCLE'] = 280
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)



bootstrap.init_app(app)
db.init_app(app)

#import models after creating db and login manager
from dscb_common.models import *
from dscb_common.views import *









#enable versioning
sa.orm.configure_mappers()


