from flask import request,jsonify, session
from flask import flash, redirect, render_template, url_for
from dscb_common.models import *
from dscb_common import app, db
from dscb_common.ldap_auth import LdapAuth
from dscb_common.forms import *
from sqlalchemy import func
import urllib

usersSessions = {}

@app.route('/login', methods=['GET', 'POST'])
def login():
    content = request.json
    sessionToken = content['sessionToken']
    username = content['username'].split("@")[0]
    password = content['password']
    response = False

    #check if already logged in during current session
    if username in usersSessions and usersSessions[username] == sessionToken:
        response = True
    else:
        if LdapAuth().authenticate(username,password) is not None:
            response = True
            usersSessions[username] = sessionToken
    return jsonify({"response":response})
        


@app.route('/')
@app.route('/users/all')
def allusers():
    all_users =  User.query.all()
    return render_template('users.html',all_users=all_users)

#page to add users:
@app.route('/users/add',methods=['GET','POST'])
def user_add():
    if request.method == 'POST':
        form = UserForm(request.form)
        if form.validate():
            user = User()
            form.populate_obj(user)
            db.session.add(user)
            db.session.commit()
            flash('Changes saved.', 'bg-success')
            return redirect("/")
        else:
            flash('Something is wrong.  Changes not saved', 'bg-danger')
            for field, e in form.errors.items():
                flash('{} - {}'.format(field, e), 'bg-danger')
            return redirect_dest()
    else:
        header = "Create New User"
        userform = UserForm()
        return render_template('form.html', action='', form=userform, header=header)



#page to edit existing users
@app.route('/users/edit/<int:user_id>',methods=['GET','POST'])
def user_edit(user_id):
    if request.method == 'POST':
        form = EditUserForm(request.form)
        if form.validate():
            p = User.query.get(user_id)
            form.populate_obj(p)
            if form.delete.data:
                newurl="/users/all"
                db.session.delete(p)
                flash('User Deleted.', 'bg-success')
                db.session.commit()
                return redirect(newurl)
            else:
                db.session.add(p)
                db.session.commit()
                flash('Changes saved.', 'bg-success')
                return redirect("/")
        else:
            flash('Something is wrong.  Changes not saved', 'bg-danger')
            for field, e in form.errors.items():
                flash('{} - {}'.format(field, e), 'bg-danger')
            return redirect_dest()
    else:
        print(user_id)
        p = User.query.get(user_id)
        print('h1')
        header = "Edit User {}'s Account for {}".format(p.username,p.app_name)
        print('h2')
        pf = EditUserForm(obj=p)
        print('h3')
        pf.submit.label.text = 'Save Edit'
        return render_template('form.html', action='', form=pf, header=header)


#all project codes page
@app.route('/projectcodes/all')
def allpcodes():
    all_codes = Pcode.query.order_by(Pcode.id).all()
    return render_template('pcodes.html',all_codes=all_codes)

