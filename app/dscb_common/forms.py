from dscb_common.models import *
from wtforms_alchemy import ModelForm, ModelFormField
import wtforms_alchemy
from flask_wtf import FlaskForm
from wtforms import FileField, SubmitField, RadioField, StringField, SelectField, SelectMultipleField, FloatField, \
    BooleanField, FieldList, FormField, HiddenField, TextAreaField, PasswordField, Form, validators
from wtforms.validators import ValidationError
from wtforms_components import IntegerField
from wtforms.fields.html5 import DateField, EmailField
from wtforms_alchemy.fields import QuerySelectField

from datetime import datetime, timedelta, date
from wtforms_components import Email
from wtforms.validators import Required, Optional, NumberRange, ValidationError

def ascii():
    message='Error, Entry contains non-Ascii Characters: '
    def _ascii(form,field):
        text=field.data
        nonascii=[]
        for char in text:
            if ord(char)>127:
               nonascii.append(char)
        if(len(nonascii)>0):
            raise ValidationError(message+str(nonascii))
    return _ascii

class LoginForm(FlaskForm):
    email = EmailField('Email', validators=[Required(),ascii()])
    password = PasswordField('Password')
    submit = SubmitField('Login')

app_choices = ["dscb_tracker","mbc_tracker","ferm_tracker"]
class UserForm(FlaskForm):
    username = StringField("username",validators=[Required(),ascii()])
    app_name = SelectField("app name", choices = [(x,x) for x in app_choices])
    #app_name = StringField("app name",validators=[Required(),ascii()])
    privelege_level = IntegerField("privelege level",validators=[Required()])
    registered_on = DateField("registered on", default= datetime.today)
    submit = SubmitField('Add User')

class EditUserForm(UserForm):
    active = BooleanField("active")
    delete = SubmitField('Delete')

