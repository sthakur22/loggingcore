##################################################
# Authenticates a username/password agains Intrexon AD.
# Usage:
#
# # This class requires the ldap3 library
# pip install ldap3 (or add this to your requirements.txt file)
# 
# # Import this file at the top of your code:
# from ldap_auth import LdapAuth
#
# # Get username and password from the user's http request or cli input.
# # For this this example, we define test values manually.
# username = 'testuser'
# password = 'secretpass' 
# 
# # Call the authenticate method
# auth_result = LdapAuth().authenticate(username, password)
# 
# # The auth_result will be None if authentication failed, or a dictionary with keys 'first_name', 'last_name', 'email'.
# if auth_result is None:
#  raise RuntimeError("Invalid username or password") # or otherwise inform the user of the login failure
# else:
#   print("%s %s - %s" % (auth_result['first_name'], auth_result['last_name'], auth_result['email']))
##################################################

from ldap3 import Server, Connection, ALL, NTLM

class LdapAuth:

  ldap_server = 'auth.intrexon.com'
  ldap_domain = 'corp'
  ldap_port = 636
  ldap_search_base = 'DC=corp,DC=intrexon,DC=local'
  ldap_field_uid = 'sAMAccountName'
  ldap_field_first = 'givenName'
  ldap_field_last = 'sn'
  ldap_field_email = 'mail'

  def authenticate(self, username, password):
    if (not username or not password): return None 
    login =  "corp\\" + username
    server = Server(self.ldap_server, port = self.ldap_port, use_ssl = True)
    with Connection(server, user = login, password = password) as conn:
      conn.open()
      result = conn.bind() # this returns True|False
      if result:
        # do a lookup on the account to get the user's info
        entry_generator = conn.extend.standard.paged_search(search_base = self.ldap_search_base,
            search_filter = '(&(objectClass=user)(' + self.ldap_field_uid + '=' + username + '))',
            search_scope = 'SUBTREE',
            attributes = [self.ldap_field_uid, self.ldap_field_first, self.ldap_field_last, self.ldap_field_email],
            paged_size = 5,
            generator=True)
        attrs = None
        for e in entry_generator:
          # The first item returned is always {'uri': ['ldaps://DomainDnsZones.corp.intrexon.local/DC=DomainDnsZones,DC=corp,DC=intrexon,DC=local'], 'type': 'searchResRef'}
          # The actual entry we want is the first one with a type of 'searchResEntry'
          try:
            if e['type'] == 'searchResEntry':
              attrs = e['attributes']
              break
          except:
            pass
        # extract to a new dict with more standardish keys and cleaned up values
        user_info = {'user_name': attrs[self.ldap_field_uid].lower(), 
          'email': attrs[self.ldap_field_email].lower(), 
          'first_name': attrs[self.ldap_field_first], 
          'last_name': attrs[self.ldap_field_last] 
        }
        return user_info

