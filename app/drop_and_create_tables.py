from dscb_common import db
from dscb_common import models
import inspect 

classes = []
for name,obj in inspect.getmembers(models):
    if inspect.isclass(obj):
        classes.append(obj)

print("dropping Users and Pcodes tables")
db.drop_all()
print("specified tables have been dropped")
print("creating specified tables")
db.create_all()
print("common_pcodes and comomon_users tables created")

