#from migrate.versioning import api
from dscb_common.config import SQLALCHEMY_DATABASE_URI
from dscb_common.config import SQLALCHEMY_MIGRATE_REPO
from dscb_common import db
import os.path
print("creating tables")
db.create_all()
print("tables created")
